var authKey = "Basic " + "ZDE4NjMwYmQyZjI5NGY1ZTg1OGU4M2Q3YmNkMjViYzk=";
var server =  "http://escamp.ongetit.com/1.1/";
var max = 1;
var siteIDs = ["beff61ce9d7b67adbb357de8332db1995c319d15",
	"ca840072abfdcbe649c6a4c1550b1371c4d5a1e3",
	"ecea05dfa995082317aa5d1a01b0029e5b5af854",
	"13c01f33d44890023ed8dc6e8ef46b0417573f59",
	"b0b3dcbb21980a597dbbc52c68ab712090b04a6a",
	"74a7b6e3cfc50bb0647fa6cb3e1f1c03d48317e8",
	"a651007ab74e59ef36e5c555c91bbd888e8ebccd",
	"247ceda8af05e53bab14a1c95da4939abc9a4e12",
	"e9e19b9a70c3bd580f42a2f8b595c44e795f9151"
	];
//{id:"beff61ce9d7b67adbb357de8332db1995c319d15", locX:6, locY:9, usage:0},
var sites = [
	{id:"ca840072abfdcbe649c6a4c1550b1371c4d5a1e3", locX:4, locY:7, usage:0},
	{id:"ecea05dfa995082317aa5d1a01b0029e5b5af854", locX:4, locY:24, usage:0},
	{id:"13c01f33d44890023ed8dc6e8ef46b0417573f59", locX:10, locY:16, usage:0},
	{id:"b0b3dcbb21980a597dbbc52c68ab712090b04a6a", locX:4, locY:13, usage:0},
	{id:"74a7b6e3cfc50bb0647fa6cb3e1f1c03d48317e8", locX:3, locY:16, usage:0},
	{id:"a651007ab74e59ef36e5c555c91bbd888e8ebccd", locX:12, locY:10, usage:0},
	{id:"247ceda8af05e53bab14a1c95da4939abc9a4e12", locX:12, locY:11, usage:0},
	{id:"e9e19b9a70c3bd580f42a2f8b595c44e795f9151", locX:5, locY:6, usage:0}
	];

$(document).ready(function() {
	var deviceID = "85e6082adea6960a89dc935b05b969ad830a35ad";
	$.each(sites, function(i, site) {
		$.ajax({
			url: server + "sites/" + site.id + "/meteringUsage",
			type: "GET",
			crossDomain: true,
			headers: {
				Authorization: authKey
			},
			data : {
				timestamp: Date.now()
			},
			success: function(data) {
				site.usage = data.meteringPeriodUsage;
				if(max < site.usage) max = site.usage;
				updateMap();
			}
		});
	});
	$("tr").each(function(i, row) {
		$("td", this).each(function(j, cell) {
			$(this).attr("id","c-"+i+"-"+j);
			if($(this).hasClass("tg-land"))
				$(this).css("background-color", heatMap(0));
		});
	});
});

function heatMapBR(value) {
	value = value.toFixed(4);
	if(value > 0.99) return "#ff0000";
	var red, green, blue;
	var colorPoints = [[0,0,255, 0], [0,255,255, 0.25], [0,255,0, 0.5], [255,255,0, 0.75], [255,0,0, 1.0]];
	var idx = parseInt((value * 4) + 1);
	var fraction = (value - colorPoints[idx-1][3]) / 0.25;
	red = parseInt(((colorPoints[idx][0] - colorPoints[idx-1][0]) * fraction + colorPoints[idx-1][0]));
	green = parseInt(((colorPoints[idx][1] - colorPoints[idx-1][1]) * fraction + colorPoints[idx-1][1]));
	blue = parseInt(((colorPoints[idx][2] - colorPoints[idx-1][2]) * fraction + colorPoints[idx-1][2]));
	red = ("0" + red.toString(16)).substr(-2);
	green = ("0" + green.toString(16)).substr(-2);
	blue = ("0" + blue.toString(16)).substr(-2);
	return "#" + red + green + blue;
}

function heatMap(value) {
	if(value > 0.99) return "#0000ff";
	value = 0.05 + value * 0.95;
	var red = parseInt(255 * (1 - value));
	var green = parseInt(255 * (1 - value));
	red = ("0" + red.toString(16)).substr(-2);
	green = ("0" + green.toString(16)).substr(-2);
	return "#" + red + green +"ff";
}

function updateMap() {
	$.each(sites, function(i, site) {
		var id = "#c-" + site.locY + "-" + site.locX;
		var ratio = site.usage  / max;
		console.log(site.locY + "," + site.locX + ":" + ratio);
		if ( ratio > 0.5 ) {
			$(id).css("color", "#ffffff");
		} else {
			$(id).css("color", "#000000");
		}
		$(id).css("background-color", heatMap(ratio));
	});
}